class afile {
    $message = lookup('afile::message')
    $node_specific_message = lookup('nodespecific::message')

    file { "/etc/somefile.txt":
        ensure  => "present",
        owner   => 'root',
        group   => 'root',
        mode    => '0644', 
        content => template("afile/somefile.erb")
    }
}

